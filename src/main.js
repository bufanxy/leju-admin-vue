import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
// import locale from 'element-ui/lib/locale/lang/en' // lang i18n

import '@/styles/index.scss' // global css

// mavonEditor  文档地址 https://github.com/hinesboy/mavonEditor
import mavonEditor from 'mavon-editor'
import 'mavon-editor/dist/css/index.css'
// use
Vue.use(mavonEditor)
import App from './App'
import store from './store'
import router from './router'
import filters from './filters' // global filters
import directives from './directive' // 全局指令
import '@/icons' // icon
import '@/permission' // permission control
import BfComponents from '@/components/index.js'
// 引入导出pdf方法
// import html2PDF from '@/utils/html2PDF'
// 引入UUID
/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online ! ! !
 */
// if (process.env.NODE_ENV === 'production') {
//     const { mockXHR } = require('../mock')
//     mockXHR()
// }

for (var key in filters) {
    Vue.filter(key, filters[key])
}
// 加载到全局
directives(Vue)
// set ElementUI lang to EN
// Vue.use(ElementUI, { locale })
// 如果想要中文版 element-ui，按如下方式声明
Vue.use(ElementUI)
// 注册全局TableCompone
// Vue.use(TableComponent)
Vue.use(BfComponents)
// Vue.use(html2PDF)
// 引入UUID
Vue.config.productionTip = false
new Vue({
    el: '#app',
    store,
    router,
    render: h => h(App)
})
