import request from '@/api/request'
// 获取素材列表
function materialListApi(start, limit) {
    return request({
        url: `/lejuAdmin/material/findMaterialByPage/${start}/${limit}`, // 相对路径
        method: 'get'

    })
}
function materialDeleteApi(id) {
    return request({
        url: `/lejuAdmin/material/delMaterial/${id}`, // 相对路径
        method: 'delete'

    })
}

// 获取文章列表
function articleListApi(start, limit, data) {
    return request({
        url: `/lejuAdmin/productArticle/findArticles/${start}/${limit}`, // 相对路径
        method: 'post',
        data

    })
}
// 新增文章
function addArticleApi(data) {
    return request({
        url: `/lejuAdmin/productArticle/addArticle`, // 相对路径
        method: 'post',
        data

    })
}
// 编辑文章
function editArticleApi(data) {
    return request({
        url: `/lejuAdmin/productArticle/updateArticle`, // 相对路径
        method: 'post',
        data

    })
}
// 删除文章
function deleteArticleApi(id) {
    return request({
        url: `/lejuAdmin/productArticle/del/${id}`, // 相对路径
        method: 'delete'

    })
}
// 文章详情
function articleDetailApi(id) {
    return request({
        url: `/lejuAdmin/productArticle/productArticle/${id}`, // 相对路径
        method: 'get'

    })
}
// 文章详情
function isShowApi(data) {
    return request({
        url: `/lejuAdmin/productArticle/changeShowStatus`, // 相对路径
        method: 'post',
        data

    })
}
// 图片上传 
function uploadFileOss(data) {
    return request({
        url: `/lejuAdmin/material/uploadFileOss`, // 相对路径
        method: 'post',
        data
    })
}
export default {
    materialListApi,
    materialDeleteApi,
    articleListApi,
    addArticleApi,
    deleteArticleApi,
    articleDetailApi,
    editArticleApi,
    isShowApi,
    
}
export {
    uploadFileOss
}
