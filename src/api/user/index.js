import request from '@/api/request'

function getUserList(start, limit) {
    return request({
        url: `/lejuAdmin/member/findMembersByPage/${start}/${limit}`, // 相对路径
        method: 'get'
    })
}

export default {
    getUserList
}
