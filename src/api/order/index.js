import request from '@/api/request'
// 訂單列表API
function orderList(start, limit, data) {
    return request({
        url: `/lejuAdmin/order/findOrdersByPage/${start}/${limit}`, // 相对路径
        method: 'post',
        data
    })
}
function orderDetail(orderId) {
    return request({
        url: `/lejuAdmin/order/orderDetail/${orderId}`, // 相对路径
        method: 'get'
    })
}

function sendOrder(data) {
    return request({
        // url: `/lejuAdmin/order/sendDone?orderId=1319204625171681282&deliverySn='123456'&deliveryCompany='顺丰'`, // 相对路径
        url: `/lejuAdmin/order/sendDone`, // 相对路径
        method: 'post',
        data
    })
}
function finishOrder(orderId) {
    return request({
        // url: `/lejuAdmin/order/sendDone?orderId=1319204625171681282&deliverySn='123456'&deliveryCompany='顺丰'`, // 相对路径
        url: `/lejuAdmin/order/finishOrder/${orderId}`, // 相对路径
        method: 'post'

    })
}
// 强制确认收货
function forceFinishOrder(orderId) {
    return request({
        // url: `/lejuAdmin/order/sendDone?orderId=1319204625171681282&deliverySn='123456'&deliveryCompany='顺丰'`, // 相对路径
        url: `/lejuAdmin/order/receiveProductsForce/${orderId}`, // 相对路径
        method: 'post'

    })
}
// 退单列表API
function orderBackList(start, limit, data) {
    return request({
        url: `/lejuAdmin/orderReturn/findReturnApply/${start}/${limit}`, // 相对路径
        method: 'post',
        data
    })
}
// 退单列表API
function orderBackDetail(id) {
    return request({
        url: `/lejuAdmin/orderReturn/${id}`, // 相对路径
        method: 'get'
    })
}
// 同意退货
function agreeReturn(id, data) {
    return request({
        url: `/lejuAdmin/orderReturn/agreeApply/${id}`, // 相对路径
        method: 'post',
        data
    })
}
// 拒绝退货
function rejectReturn(id, data) {
    return request({
        url: `/lejuAdmin/orderReturn/rejectApply/${id}`, // 相对路径
        method: 'post',
        data
    })
}
// 确认收货
function receiveReturn(id, data) {
    return request({
        url: `/lejuAdmin/orderReturn/receiveProduct/${id}`, // 相对路径
        method: 'post',
        data
    })
}
// 同意退货
export default {
    orderList,
    orderDetail,
    sendOrder,
    finishOrder,
    orderBackList,
    orderBackDetail,
    agreeReturn,
    rejectReturn,
    receiveReturn,
    forceFinishOrder
}
