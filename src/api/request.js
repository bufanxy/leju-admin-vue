import axios from 'axios'
import { Message } from 'element-ui'
import { getToken, clearUserCache, getUserInfo } from '@/utils/myAuth'
import router from '@/router'
// import md5 from "md5"
// create an axios instance
// process.env 指的是当前的运行环境 所对应的 配置文件
var host
if (process.env.NODE_ENV == 'development') {
    host = ''
} else {
    host = 'https://leju.bufan.cloud'
}
const service = axios.create({
    // 前端独立部署 才需要处理
    baseURL: host // url = base url + request url
    // withCredentials: true // 跨域携带cookies需要,token认证不需要
    // timeout: 5000 // 请求超时时间
})

// request interceptor
service.interceptors.request.use(
    config => {
        // do something before request is sent
        // 如果本地有token  携带过去
        var token = getToken()
        // if (data === FormData) {
        //     config.headers['Content-Type'] = 'formdata'
        // } else {
        //     config.headers['Content-Type'] = 'application/json;charset=UTF-8'
        // }

        // if(config.data){
        //      username passwords
        // }
        if (token) {
            config.headers['token'] = token
        }

        return {
            ...config
            // cancelToken 属于axios的config配置,可以用来提前终止当前请求.在回调里调用cancel()方法实现终止. 不调用cancel()不会终止.
            // cancelToken: new axios.CancelToken((cancel) => {
            //     // 添加auth的控制 避免对权限数据进行删改
            //     const filterMethods = ['put', 'delete']
            //     if (getUserInfo()?.username === 'admin' &&
            //         config.url.indexOf('sysAuth') !== -1 &&
            //         filterMethods.includes(config.method.toLowerCase())) {
            //         Message({
            //             message: 'admin权限数据不能被修改!',
            //             type: 'error',
            //             duration: 5 * 1000
            //         })
            //         cancel('Cancel not allowed request!')
            //     }
            // })
        }
    },
    error => {
        // do something with request error
        console.log(error) // for debug
        return Promise.reject(error)
    }
)

// response interceptor
service.interceptors.response.use(
    response => {
        const res = response.data
        // token无效 会返回20002
        if (res.code === 20002) {
            Message({
                message: res.message + ',请重新登陆!',
                type: 'error',
                duration: 5 * 1000
            })
            // 清除用户缓存
            clearUserCache()
            router.push('/login')
        }

        return res
    },
    error => {
        console.log('err' + error) // for debug
        Message({
            message: error.message,
            type: 'error',
            duration: 5 * 1000
        })
        return Promise.reject(error || '请求错误')
    }
)
/**
 * 合并get与post提交
 * @param {} config
 */
function http(config) {
    // 动手脚
    const axiosConfig = {
        url: config.url,
        method: config.method
    }
    // 如果data存在 而且不为空对象
    if (config.data) {
        if (Object.keys(config.data).length > 0) {
            var key = config.method.toLowerCase() === 'get' ? 'params' : 'data'
            axiosConfig[key] = config.data
        }
        if (config.data.constructor === FormData) {
            axiosConfig.data = config.data
        }
    }

    if (config.headers) {
        axiosConfig.headers = config.headers
    }
    return service(axiosConfig)
}
export default http

