import request from '@/api/request'
// 获取分类列表
function getCategory() {
    return request({
        url: `/lejuAdmin/category/getAllCategory`, // 相对路径
        method: 'get'
    })
}
// 修改分类
function updateCategory(data) {
    console.log('sdfsd')
    return request({
        url: `/lejuAdmin/category/updateCategory`, // 相对路径
        method: 'post',
        data
    })
}
// 新增分类
function addCategory(data) {
    console.log('sdfsd')
    return request({
        url: `/lejuAdmin/category/addCategory`, // 相对路径
        method: 'post',
        data
    })
}
export default {
    getCategory,
    updateCategory,
    addCategory
}
