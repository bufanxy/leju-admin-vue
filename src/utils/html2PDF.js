// 问题1 如何在window.print 跳到新url打印
// 问题2 宽度超过A4时候 要修改ele的宽度,不然显示不全
export default {
    install(Vue) {
        Vue.prototype.$ = (ele) => {
            var newstr = ele
            var oldstr = document.body.innerHTML

            document.body.innerHTML = ''
            document.body.appendChild(newstr)

            window.print()

            document.body.innerHTML = oldstr
        }
    }
}
