module.exports = {

    title: '乐居商城后台管理系统',

    /**
     * @type {boolean} true | false
     * @description Whether fix the header
     */
    fixedHeader: false,

    /**
     * @type {boolean} true | false
     * @description Whether show the logo in sidebar
     */
    sidebarLogo: false,
    // 测试地址: http://bufantec.com
    // host: 'http://localhost'
    host: 'https://leju.bufan.cloud'
}
