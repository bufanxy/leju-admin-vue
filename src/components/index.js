import myTableComponent from '@/components/tableComponent/index.vue'
import BtnPermission from '@/components/BtnPermission/index.vue'
import OrderTable from '@/components/OrderTable/index'
import BfCollapse from '@/components/BfCollapse/index'
import BfProgress from '@/components/BfProgress/index'
const bfComponents = {
    install: function(Vue) {
        Vue.component('TableComponent', myTableComponent)
        Vue.component('BtnPermission', BtnPermission)
        Vue.component('OrderTable', OrderTable)
        Vue.component('BfCollapse', BfCollapse)
        Vue.component('BfProgress', BfProgress)
    }
}
export default bfComponents
